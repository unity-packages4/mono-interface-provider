# Mono Interface Provider

Механизм доступа интерфейсов в инспекторе Unity.

###Пример использования
```c#
// интерфейс
public interface IInterface {
    public int Property;
}

// Провайдер для доступа
[Serializable]
public class InterfaceProvider : MonoInterfaceProvider<IInterface> {}


// класс, который будет хранить поле для реализаций интерфейса
public class Widget : MonoBehaviour
{
    [SerializeField] private InterfaceProvider _interface;
}

// реализация интерфейса, который будет подставляться в поле инспектора
public class InterfaceWidget: MonoBehaviour, IInterface {}
public class InterfaceAnotherWidget: MonoBehaviour, IInterface {}
```