using System;
using UnityEngine;

namespace MonoInterface.Runtime
{
    [Serializable]
    public class MonoInterfaceProvider<T>
    {
        [SerializeField] private GameObject _interfaceObject;
        private T _interface;

        public T Value
        {
            get
            {
                if (_interface == null)
                {
                    _interface = _interfaceObject.GetComponent<T>();
                }
                return _interface;
            }
        }
    }
}